<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
	$soapClient = new SoapClient('Shipping.wsdl');
	echo '<pre>';
	print_r($soapClient->__getFunctions());

	$params = array(
			'Shipments' => array(
				'Shipment' => array(
						'Shipper'	=> array(
										'Reference1' 	=> 'Ref test/232323',
										'Reference2' 	=> '',
										'AccountNumber' => '36672161',
										'PartyAddress'	=> array(
											'Line1'				=> 'Mecca St',
											'Line2' 			=> '',
											'Line3' 			=> '',
											'City'			 	=> '',
											'StateOrProvinceCode'		=> '',
											'PostCode'			=> '400007',
											'CountryCode'			=> 'IN'
										),
										'Contact'		=> array(
											'Department'			=> 'test',
											'PersonName'			=> 'test',
											'Title'				=> '',
											'CompanyName'			=> 'Aramex',
											'PhoneNumber1'			=> '5555555',
											'PhoneNumber1Ext'		=> '125',
											'PhoneNumber2'			=> '',
											'PhoneNumber2Ext'		=> '',
											'FaxNumber'			=> '',
											'CellPhone'			=> '07777777',
											'EmailAddress'			=> 'test@test.com',
											'Type'				=> ''
										),
						),
												
						'Consignee'	=> array(
										'Reference1'	=> '',
										'Reference2'	=> '',
										'AccountNumber' => '',
										'PartyAddress'	=> array(
											'Line1'				=> '15 ABC St',
											'Line2'				=> '',
											'Line3'				=> '',
											'City'				=> '',
											'StateOrProvinceCode'		=> '',
											'PostCode'			=> '400007',
											'CountryCode'			=> 'IN'
										),
										
										'Contact'		=> array(
											'Department'			=> 'test',
											'PersonName'			=> 'test',
											'Title'				=> '',
											'CompanyName'			=> 'Aramex',
											'PhoneNumber1'			=> '6666666',
											'PhoneNumber1Ext'		=> '155',
											'PhoneNumber2'			=> '',
											'PhoneNumber2Ext'		=> '',
											'FaxNumber'			=> '',
											'CellPhone'			=> '123456',
											'EmailAddress'			=> 'test@test.com',
											'Type'				=> ''
										),
						),
						
						'ThirdParty' => array(
										'Reference1' 	=> '',
										'Reference2' 	=> '',
										'AccountNumber' => '',
										'PartyAddress'	=> array(
											'Line1'				=> '',
											'Line2'				=> '',
											'Line3'				=> '',
											'City'				=> '',
											'StateOrProvinceCode'		=> '',
											'PostCode'			=> '',
											'CountryCode'			=> ''
										),
										'Contact'		=> array(
											'Department'			=> '',
											'PersonName'			=> '',
											'Title'				=> '',
											'CompanyName'			=> '',
											'PhoneNumber1'			=> '',
											'PhoneNumber1Ext'		=> '',
											'PhoneNumber2'			=> '',
											'PhoneNumber2Ext'		=> '',
											'FaxNumber'			=> '',
											'CellPhone'			=> '',
											'EmailAddress'			=> '',
											'Type'				=> ''						
										),
						),
						
						'Reference1' 				=> '',
						'Reference2' 				=> '',
						'Reference3' 				=> '',
						'ForeignHAWB'				=> '',
						'TransportType'				=> 0,
						'ShippingDateTime' 			=> time(),
						'DueDate'				=> time(),
						'PickupLocation'			=> 'Reception',
						'PickupGUID'				=> '',
						'Comments'				=> 'Shpt 0001',
						'AccountingInstrcutions' 		=> '',
						'OperationsInstructions'		=> '',
						
						'Details' => array(
										'Dimensions' => array(
											'Length'=> 0,
											'Width'	=> 0,
											'Height'=> 0,
											'Unit'	=> 'cm',
											
										),
										
										'ActualWeight' => array(
											'Value'					=> 0.5,
											'Unit'					=> 'Kg'
										),

										'ChargeableWeight' => array(
											'Value'					=> 0.5,
											'Unit'					=> 'Kg'
										),		
										
										'ProductGroup' 			=> 'DOM',
										'ProductType'			=> 'CDA',
										'PaymentType'			=> 'P',
										'PaymentOptions' 		=> '',
										'Services'			=> 'CODS',
										'NumberOfPieces'		=> 1,
										'DescriptionOfGoods' 		=> 'Docs',
										'GoodsOriginCountry' 		=> 'IN',
										
										'CashOnDeliveryAmount' 	=> array(
										'Value'				=> 10,
										'CurrencyCode'			=> 'INR'
											),
										
										'InsuranceAmount'		=> Null,
										
										'CollectAmount'			=> NUll,
										
										'CashAdditionalAmount'		=> Null,
										
										'CashAdditionalAmountDescription' => Null,
										
										'CustomsValueAmount' 		=> Null,
										
										'Items' 			=> array(
											
										)
						),
				),
			),


		

			'ClientInfo'  			=> array      ( 'AccountCountryCode'	=> 'IN',
									'AccountEntity'		=> 'BOM',
									'AccountNumber'		=> '36672161',
									'AccountPin'		=> '115216',
									'UserName'		=> 'sidd@noesis369.com',
									'Password'		=> 'BlueSky77',
									'Version'		=> 'v1.0'
									),

			'Transaction' 			=> array(
										'Reference1'			=> '001',
										'Reference2'			=> '', 
										'Reference3'			=> '', 
										'Reference4'			=> '', 
										'Reference5'			=> '',									
									),
			'LabelInfo'				=> array(
										'ReportID' 			=> 9729,
										'ReportType'			=> 'URL',
			),

);

	
	$params['Shipments']['Shipment']['Details']['Items'][] = array(
		'PackageType' 	=> 'Box',
		'Quantity'		=> 1,
		'Weight'		=> array(
				'Value'		=> 0.5,
				'Unit'		=> 'Kg',		
		),
		'Comments'		=> 'Docs',
		'Reference'		=> ''
	);
	
	print_r($params);
	try {

		$auth_call = $soapClient->CreateShipments($params);
		echo '<pre>';
		print_r($auth_call);
		
		die();
	} catch (SoapFault $fault) {
		die('Error : ' . $fault->faultstring);
	}
?>
<?php
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
	$soapClient = new SoapClient('Shipping.wsdl');
	echo '<pre>';
	print_r($soapClient->__getFunctions());

	
	$params = array(
			'Pickup' => array(
								'PickupAddress'	=> array(
											'Line1'			=> 'Test',
											'Line2' 		=> '',
											'Line3' 		=> '',
											'City'			=> '',
											'StateOrProvinceCode'	=> '',
											'PostCode'		=> '400093',
											'CountryCode'		=> 'IN'
										),
								'PickupContact'		=> array(
											'Department'			=> 'IT',
											'PersonName'			=> 'Michael',
											'Title'				=> '',
											'CompanyName'			=> 'Aramex',
											'PhoneNumber1'			=> '5555555',
											'PhoneNumber1Ext'		=> '125',
											'PhoneNumber2'			=> '',
											'PhoneNumber2Ext'		=> '',
											'FaxNumber'			=> '',
											'CellPhone'			=> '07777777',
											'EmailAddress'			=> 'test@test.com',
											'Type'				=> ''
										),
								'PickupLocation' 			=> 'Reception',
								'PickupDate' 				=> '2015-10-22T15:00:00',
								'ReadyTime' 				=> '2015-10-22T15:00:00',
								'LastPickupTime'			=> 1428076800,
								'ClosingTime'				=> 1428076800 + 1800,
								'ShippingDateTime' 			=>  '2015-10-22',
								'Comments'				=>  '',
								'Reference1'				=> 'test',
								'Reference2'				=> '',
								'Vehicle'				=> '',
								'Status' 				=> 'Ready',
								
								'PickupItems' 			=> array(
									
									'PickupItemDetail' 			=> array(
											'ProductGroup' 			=> 'DOM',
											'ProductType'			=> 'ONP',
											'Payment'			=> 'P',
											'NumberOfShipments' 		=> 1,
											'PackageType'			=> '',
											'NumberOfPieces'		=> 1,
											'Comments' 			=> '',
											'ShipmentWeight' => array(
													'Value'		=> 0.5,
													'Unit'		=> 'Kg'
														),
											'ShipmentVolume' => array(
													'Value'		=> 0.5,
													'Unit'		=> 'Kg'
														),
											'CashAmount' 	=> array(
													'Value'		=> 0,
													'CurrencyCode'	=> ''
														),
											'ExtraCharges' 	=> array(
													'Value'		=> 0,
													'CurrencyCode'	=> ''
														),
											'ShipmentDimensions' => array(
													'Length'	=> 0,
													'Width'		=> 0,
													'Height'	=> 0,
													'Unit'		=> 'cm',
											
														),
										),
									),
								),

			'ClientInfo'  					=> array(
										'AccountCountryCode'	=> 'IN',
										'AccountEntity'		=> 'BOM',
										'AccountNumber'		=> '36672161',
										'AccountPin'		=> '115216',
										'UserName'		=> 'sidd@noesis369.com',
										'Password'		=> 'BlueSky77',
										'Version'		=> 'v1.0'

									),

			'Transaction' 			=> array(
										'Reference1'			=> 'test',
										'Reference2'			=> '', 
										'Reference3'			=> '', 
										'Reference4'			=> '', 
										'Reference5'			=> '',									
									),
			'LabelInfo'				=> Null,
	);
	
	print_r($params);
	
	try {
		$auth_call = $soapClient->CreatePickup($params);
		echo '<pre>';
		print_r($auth_call);
		die();
	} catch (SoapFault $fault) {
		die('Error : ' . $fault->faultstring);
	}
?>

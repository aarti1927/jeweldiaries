<?php
/**
 * The theme header
 *
 * */
global $cg_options;
$protocol = (!empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https:" : "http:";

$cg_responsive_status = '';

if ( isset( $cg_options['cg_responsive'] ) ) {
	$cg_responsive_status = $cg_options['cg_responsive'];
}

$cg_logo = '';

$cg_favicon = '';

if ( isset( $cg_options['cg_favicon']['url'] ) ) {
	$cg_options['cg_favicon']['url'] = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_favicon']['url'] );
	$cg_favicon						 = $cg_options['cg_favicon']['url'];
}

$cg_retina_favicon = '';

if ( isset( $cg_options['cg_retina_favicon']['url'] ) ) {
	$cg_options['cg_retina_favicon']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_retina_favicon']['url'] );
	$cg_retina_favicon						 = $cg_options['cg_retina_favicon']['url'];
}

$cg_topbar_display = '';

if ( isset( $cg_options['cg_topbar_display'] ) ) {
	$cg_topbar_display = $cg_options['cg_topbar_display'];
}

$cg_topbar_message = '';

if ( isset( $cg_options['cg_topbar_message'] ) ) {
	$cg_topbar_message = $cg_options['cg_topbar_message'];
}

$cg_display_cart = '';

if ( isset( $cg_options['cg_show_cart'] ) ) {
	$cg_display_cart = $cg_options['cg_show_cart'];
}

$cg_catalog = '';

if ( isset( $cg_options['cg_catalog_mode'] ) ) {
	$cg_catalog = $cg_options['cg_catalog_mode'];
}

$cg_primary_menu_layout = '';

if ( isset( $cg_options['cg_primary_menu_layout'] ) ) {
	$cg_primary_menu_layout = $cg_options['cg_primary_menu_layout'];
}

$cg_sticky_menu = '';

if ( isset( $cg_options['cg_sticky_menu'] ) ) {
	$cg_sticky_menu = $cg_options['cg_sticky_menu'];
}

if ( !empty( $_SESSION['cg_header_top'] ) ) {
	$cg_topbar_display = $_SESSION['cg_header_top'];
}

$shop_announcements = '';

if ( isset( $cg_options['cg_shop_announcements'] ) ) {
	$shop_announcements = $cg_options['cg_shop_announcements'];
}

$logo_position = '';

if ( isset( $cg_options['cg_logo_position'] ) ) {
	$logo_position = $cg_options['cg_logo_position'];
}

if ( isset( $_GET['logo_position'] ) ) {
	$logo_position = $_GET['logo_position'];
}

?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="no-js ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
		<?php
		if ( $cg_responsive_status == 'enabled' ) {
			?>
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php } ?>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">        
        <link rel="shortcut icon" href="<?php
		if ( $cg_favicon ) {
			echo esc_url( $cg_favicon );
		} else {
			?><?php echo get_template_directory_uri(); ?>/favicon.png<?php } ?>"/>

        <link rel="apple-touch-icon-precomposed" href="<?php
		if ( $cg_retina_favicon ) {
			echo esc_url( $cg_retina_favicon );
		} else {
			?><?php echo get_template_directory_uri(); ?>/apple-touch-icon-precomposed.png<?php } ?>"/>
       <!--[if lte IE 9]><script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script><![endif]-->
		<?php wp_head(); ?>
		<?php
			 global $WOOCS;
			 if(isset($WOOCS->current_currency))
			 {
				 ?>
				 <script>
					 var current_currency="<?php echo $WOOCS->current_currency; ?>";
					 </script>
				 <?php
			 }
			?>
			<script>(function() { var a = document.createElement("script");a.type = "text/javascript"; a.async = true;a.src=("https:"==document.location.protocol?"https://":"http://cdn.")+"chuknu.sokrati.com/18319/tracker.js";var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(a, s); })(); </script> 
			<!-- ViralMint JS -->
			<script type="text/javascript">
			 window.vrlmnt = (function(d, s, id) {
			 var js, vjs = d.getElementsByTagName(s)[0];
			 if (d.getElementById(id)) return; js = d.createElement(s);
			 js.src = "//cdn.viralmint.com/js/viralmint-min.js";
			  js.id = id; js.acc_id = "1475797701";
			 vjs.parentNode.insertBefore(js, vjs);
			 return window.vrlmnt || (v = { _e: [], ready: function(f){v._e.push(f)}});
			 }(document, "script", "viralmint-js"));
			</script>
			<!-- ViralMint JS -->
		    </head>
    <body id="skrollr-body" <?php body_class(); ?>>


<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T5FMKW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-T5FMKW');</script>
<!-- End Google Tag Manager -->

		<?php if ( $logo_position == 'left' ) { ?>
			<?php get_template_part( 'partials/header', 'left' ); ?>

		<?php } else if ( $logo_position == 'beside' ) { ?>
			<?php get_template_part( 'partials/header', 'logoandmenubeside' ); ?>

		<?php } else if ( $logo_position == 'center' ) { ?>
			<?php get_template_part( 'partials/header', 'center' ); ?>

		<?php } else if ( $logo_position == 'center-logo-center-menu' ) { ?>
			<?php get_template_part( 'partials/header', 'centerlogocentermenu' ); ?>

		<?php } else { ?>
			<?php get_template_part( 'partials/header', 'logoandmenubeside' ); ?>
		<?php } ?>


		<?php
		if ( $cg_responsive_status !== 'disabled' ) {
			?>
			<div id="mobile-menu">
				<a id="skip" href="#cg-page-wrap" class="hidden" title="<?php esc_attr_e( 'Skip to content', 'commercegurus' ); ?>"><?php _e( 'Skip to content', 'commercegurus' ); ?></a> 
				<?php
				if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'mobile' ) ) {
					wp_nav_menu( array( 'theme_location' => 'mobile', 'container' => 'ul', 'menu_id' => 'mobile-cg-mobile-menu', 'menu_class' => 'mobile-menu-wrap', 'walker' => new mobile_cg_menu() ) );
				} elseif ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'primary' ) ) {
					wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'ul', 'menu_id' => 'mobile-cg-primary-menu', 'menu_class' => 'mobile-menu-wrap', 'walker' => new mobile_cg_menu() ) );
				}
				?>
			</div><!--/mobile-menu -->
<?php } ?>

		<div id="cg-page-wrap" class="hfeed site">
			<?php do_action( 'before' ); ?>
			<?php if ( is_wc_active() ) { ?> 
				<?php if ( function_exists( 'wc_print_notices' ) ) { ?>
					<?php
					$cg_wc_notices = WC()->session->get( 'wc_notices', array() );
					if ( !empty( $cg_wc_notices ) ) {
						?>
						<div class="cg-wc-messages">
							<div class="container">
								<div class="row">
									<div class="col-lg-12">
			<?php wc_print_notices(); ?>
									</div>
								</div>
							</div>
						</div>
						<?php
					}
				}
				?>
<?php } ?>
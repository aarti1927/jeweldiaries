<?php
/**
 * Checkout Form
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.3.0
 */
if ( !defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( !$checkout->enable_signup && !$checkout->enable_guest_checkout && !is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() );
?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( $get_checkout_url ); ?>" enctype="multipart/form-data">

	<?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
		
			
			<div class="col2-set" id="customer_details">
				<div id="stickContainer" class="wrapperStick">
				<div class="col-lg-6 col-md-6 col-sm-6" id="left-main">
					<?php do_action( 'woocommerce_checkout_billing' ); ?>
					<?php do_action( 'woocommerce_checkout_shipping' ); ?>
				</div>

				<div class="col-lg-6 col-md-6 col-sm-6 wrapper-right " id="checkout_order">

					<div class="order-wrap">
						<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

						<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

					<?php endif; ?>

					<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

                    <div id="order_review" class="woocommerce-checkout-review-order">
						<?php do_action( 'woocommerce_checkout_order_review' ); ?>
                    </div>

					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

				</div>

			</div>
				</div>
		</div>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>

<script>
	
/*
jQuery(window).scroll(function() {
		
        var styledDiv = jQuery('.footercontainer');
         var targetScroll = jQuery('#checkout_order').position().top;
         var currentScroll =  jQuery('body').scrollTop();
		 var footertotop=jQuery('.footercontainer').position().top;
		 var adtobottom=jQuery('#checkout_order').position().bottom;


     if (currentScroll>600) {
      jQuery('#checkout_order').css({position:"static ",top:"10px ",right:"0"});


    } else {
      if (currentScroll<=600) {
       jQuery('#checkout_order').css({position:"relative ",top:""});
 
      }
    }
    
    if (jQuery(window).scrollTop() + jQuery(window).height() > footertotop) {

jQuery('#checkout_order').css('margin-top',  10);
}

else  {
jQuery('#checkout_order').css('margin-top', 0);
}

		});
*/
	
	
/*
jQuery(document).ready(function(){
	//jQuery('#checkout_order').stickyMojo({footerID: '#footer', contentID: '#left-main'});
	jQuery(document.body).trigger('sticky_kit:recalc');
	jQuery("#checkout_order").stick_in_parent({container: jQuery("#stickContainer"), offset_top: 30});
	
});
*/
	
/*
jQuery(window).scroll(function(){
	  console.log(pos.top);
      if (jQuery(this).scrollTop() >= pos.top) {
	      	      
       jQuery('#checkout_order').removeClass('stick_top');
       jQuery('#checkout_order').addClass('stick_bottom');
      } else {
	       
        jQuery('#checkout_order').addClass('stick_top');
      jQuery('#checkout_order').removeClass('stick_bottom');
      
      }
  });	
});*/
	
</script>	
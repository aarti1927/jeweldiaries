<?php
// Logo to the left with menu below

global $cg_options;
$protocol = (!empty( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https:" : "http:";

$cg_responsive_status = '';

if ( isset( $cg_options['cg_responsive'] ) ) {
	$cg_responsive_status = $cg_options['cg_responsive'];
}

$cg_logo = '';

$cg_favicon = '';

if ( isset( $cg_options['cg_favicon']['url'] ) ) {
	$cg_options['cg_favicon']['url'] = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_favicon']['url'] );
	$cg_favicon						 = $cg_options['cg_favicon']['url'];
}

$cg_retina_favicon = '';

if ( isset( $cg_options['cg_retina_favicon']['url'] ) ) {
	$cg_options['cg_retina_favicon']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_retina_favicon']['url'] );
	$cg_retina_favicon						 = $cg_options['cg_retina_favicon']['url'];
}

$cg_topbar_display = '';

if ( isset( $cg_options['cg_topbar_display'] ) ) {
	$cg_topbar_display = $cg_options['cg_topbar_display'];
}

$cg_topbar_message = '';

if ( isset( $cg_options['cg_topbar_message'] ) ) {
	$cg_topbar_message = $cg_options['cg_topbar_message'];
}

$cg_display_cart = '';

if ( isset( $cg_options['cg_show_cart'] ) ) {
	$cg_display_cart = $cg_options['cg_show_cart'];
}

$cg_catalog = '';

if ( isset( $cg_options['cg_catalog_mode'] ) ) {
	$cg_catalog = $cg_options['cg_catalog_mode'];
}

$cg_primary_menu_layout = '';

if ( isset( $cg_options['cg_primary_menu_layout'] ) ) {
	$cg_primary_menu_layout = $cg_options['cg_primary_menu_layout'];
}

$cg_sticky_menu = '';

if ( isset( $cg_options['cg_sticky_menu'] ) ) {
	$cg_sticky_menu = $cg_options['cg_sticky_menu'];
}

if ( !empty( $_SESSION['cg_header_top'] ) ) {
	$cg_topbar_display = $_SESSION['cg_header_top'];
}

$shop_announcements = '';

if ( isset( $cg_options['cg_shop_announcements'] ) ) {
	$shop_announcements = $cg_options['cg_shop_announcements'];
}

$logo_position = '';

if ( isset( $cg_options['cg_logo_position'] ) ) {
	$logo_position = $cg_options['cg_logo_position'];
}
?>

<div id="wrapper" class="cg-wrapper-gap cg-fixed">

			<?php
								if ( !empty( $cg_options['site_logo']['url'] ) ) {
									$cg_options['site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['site_logo']['url'] );
									$cg_logo						 = $cg_options['site_logo']['url'];
								}
								if ( !empty( $_SESSION['cg_skin_color'] ) ) {
									$cg_skin_color = $_SESSION['cg_skin_color'];
									if ( $cg_skin_color == '#DF440B' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_red.png';
									} elseif ( $cg_skin_color == '#1e73be' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_blue.png';
									} elseif ( $cg_skin_color == '#519d42' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_green.png';
									} elseif ( $cg_skin_color == '#9b3b85' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_purple.png';
									}
								}

								if ( $cg_logo ) {
									$cg_logo_width		 = $cg_options['site_logo']['width'];
									$cg_logo_max_width	 = $cg_logo_width / 2;
								
								if ( isset( $_GET['logo2'] ) ) {
									if ( !empty( $cg_options['cg_alt_site_logo']['url'] ) ) {
										$cg_options['cg_alt_site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_alt_site_logo']['url'] );
										$cg_logo						 = $cg_options['cg_alt_site_logo']['url'];
										}

									if ( $cg_logo ) {
										$cg_logo_width		 = $cg_options['cg_alt_site_logo']['width'];
										$cg_logo_max_width	 = $cg_logo_width / 2;
									}
								}
								?>
	                                <div class="container logo image headerlogo">
		                                <div class="free_shipping_header"><p class="free"> Free Shipping Across India</p></div>
	                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">

	                                        <span class="helper"></span><img src="<?php echo esc_url( $cg_logo ); ?>" style="max-width: <?php echo esc_attr( $cg_logo_max_width ); ?>px;" alt="<?php bloginfo( 'name' ); ?>"/><h4 class="tagline" style="font-weight: inherit;font-family:lato;font-size: 20px;">THE ULTIMATE IN STYLISH FASHION JEWELLERY
</h4></a>
	                                    <div class="cash_header"><p class="">Cash on delivery</p></div>
	                                </div>
								<?php } else { ?>
	                                <div class="logo text-logo">
	                                    <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
	                                </div>
								<?php } ?>
	        <!-- Default Logo to the left with menu beside -->
	<div class="cg-menu-beside cg-wrapper-gap">
	
	 <div class="container">
			<div class="cg-logo-cart-wrap">
				<div class="cg-logo-inner-cart-wrap">
					<div class="row">
						<div class="container">
							<div class="cg-wp-menu-wrapper">
								<?php if ( $cg_responsive_status !== 'disabled' ) { ?>
	                                <div id="load-mobile-menu">
	                                </div>
								<?php } ?>
								<div class="cg-beside-search visible-lg">
									<?php echo cg_product_search(); ?>
								</div>
								
					<!--			<?php
								if ( !empty( $cg_options['site_logo']['url'] ) ) {
									$cg_options['site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['site_logo']['url'] );
									$cg_logo						 = $cg_options['site_logo']['url'];
								}
								if ( !empty( $_SESSION['cg_skin_color'] ) ) {
									$cg_skin_color = $_SESSION['cg_skin_color'];
									if ( $cg_skin_color == '#DF440B' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_red.png';
									} elseif ( $cg_skin_color == '#1e73be' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_blue.png';
									} elseif ( $cg_skin_color == '#519d42' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_green.png';
									} elseif ( $cg_skin_color == '#9b3b85' ) {
										$cg_logo = CG_THEMEURI . '/images/logo_purple.png';
									}
								}

								if ( $cg_logo ) {
									$cg_logo_width		 = $cg_options['site_logo']['width'];
									$cg_logo_max_width	 = $cg_logo_width / 2;
								
								if ( isset( $_GET['logo2'] ) ) {
									if ( !empty( $cg_options['cg_alt_site_logo']['url'] ) ) {
										$cg_options['cg_alt_site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_alt_site_logo']['url'] );
										$cg_logo						 = $cg_options['cg_alt_site_logo']['url'];
										}

									if ( $cg_logo ) {
										$cg_logo_width		 = $cg_options['cg_alt_site_logo']['width'];
										$cg_logo_max_width	 = $cg_logo_width / 2;
									}
								}

								?>

	                                <div class="logo image">
	                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
	                                        <span class="helper"></span><img src="<?php echo esc_url( $cg_logo ); ?>" style="max-width: <?php echo esc_attr( $cg_logo_max_width ); ?>px;" alt="<?php bloginfo( 'name' ); ?>"/></a>
	                                </div>

								<?php } else { ?>
	                                <div class="logo text-logo">
	                                    <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
	                                </div>
								<?php } ?>-->

							</div>

							<!-- start cg-primary-menu -->
							<div class="cg-primary-menu-beside">
								<div class="cg-primary-menu cg-wp-menu-wrapper cg-primary-menu-below-wrapper">
									<div class="container">
										<div class="row">
											<?php if ( has_nav_menu( 'primary' ) ) { ?>
												<?php
												wp_nav_menu( array(
													'theme_location'	 => 'primary',
													'before'			 => '',
													'after'				 => '',
													'link_before'		 => '',
													'link_after'		 => '',
													'depth'				 => 4,
													'container'			 => 'div',
														'container_class'	 => 'cg-main-menu',
													'fallback_cb'		 => false,
													'walker'			 => new primary_cg_menu() )
												);
												?>
											<?php } else { ?>
												<p class="setup-message">You can set your main menu in <strong>Appearance &gt; Menus</strong></p>
											<?php } ?>
											<?php if ( $cg_display_cart !== 'no' ) { ?>
												<?php if ( $cg_catalog == 'disabled' ) { ?>
													<div class="cart-wrap">
														<?php
														if ( class_exists( 'WooCommerce' ) ) {
															?>
															<?php echo cg_woocommerce_cart_dropdown(); ?>
														<?php }
														?>
													</div>
												<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							<!-- end cg-primary-menu -->

						</div><!--/container -->
					</div><!--/row -->
				</div><!--/cg-logo-inner-cart-wrap -->
			</div><!--/cg-logo-cart-wrap -->
		</div><!--/container -->
	</div><!--/cg-menu-beside -->

	<div class="cg-shopping-toolbar cg-toolbar-fixed">
    <div class="container">
        <div class="row">
            <div class="top-bar-left col-sm-12 col-md-6 col-lg-9">
				<?php if ( is_active_sidebar( 'top-bar-left' ) ) : ?>
					<?php dynamic_sidebar( 'top-bar-left' ); ?>
				<?php endif; ?>
            </div>
            <div class="col-sm-12 col-md-3 col-lg-3 visible-md visible-sm visible-xs mobile-search">
				<?php if ( is_active_sidebar( 'mobile-search' ) ) : ?>
					<?php dynamic_sidebar( 'mobile-search' ); ?>
				<?php endif; ?>
            </div>
            <div class="col-sm-3 col-md-3 col-lg-3 top-bar-right wpml cg-top-menu-beside">

	            <div class="currency_switcher"><?php echo do_shortcode('[woocs]'); ?></div>
				<?php if ( is_active_sidebar( 'top-bar-right' ) ) : ?>
					<?php dynamic_sidebar( 'top-bar-right' ); ?>
				<?php endif; ?>

				<?php
				if ( $cg_display_cart !== 'no' ) {
					if ( $cg_catalog == 'disabled' ) {
						echo cg_header_cart();
					}
				}
				?>


            </div>

        </div>
    </div>
</div>


<?php
if ( $cg_sticky_menu == 'yes' ) {
	?>
	<!--FIXED -->
	<?php 
	
	$cg_fixed_beside_style = '';

	if ( isset( $_GET['logo_position'] ) ) {
		$logo_position = $_GET['logo_position'];
	}
	
	if ( $logo_position == 'beside' ) { 
		$cg_fixed_beside_style = 'cg-fixed-beside';
	} ?>
	<div class="cg-header-fixed-wrapper <?php echo esc_attr( $cg_fixed_beside_style ); ?>">
		<div class="cg-header-fixed">
			<div class="container">
				<div class="cg-wp-menu-wrapper">
					<div class="cg-primary-menu">
						<div class="row">
							<div class="container">
								<div class="shipping_cash">
									<p class="sticky_free"> Free Shipping Across India</p>
											<?php
									if ( isset( $cg_options['site_logo']['url'] ) ) {
										$cg_options['site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['site_logo']['url'] );
										$cg_logo						 = $cg_options['site_logo']['url'];
									}

									if ( $cg_logo ) {
										$cg_logo_width		 = $cg_options['site_logo']['width'];
										$cg_logo_max_width	 = $cg_logo_width / 2;

										if ( isset( $_GET['logo2'] ) ) {
											if ( !empty( $cg_options['cg_alt_site_logo']['url'] ) ) {
												$cg_options['cg_alt_site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_alt_site_logo']['url'] );
												$cg_logo						 = $cg_options['cg_alt_site_logo']['url'];
												}

											if ( $cg_logo ) {
												$cg_logo_width		 = $cg_options['cg_alt_site_logo']['width'];
												$cg_logo_max_width	 = $cg_logo_width / 2;
											}
										}
										?>
										<div class="logo image">
											<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" style="max-width: <?php echo esc_attr( $cg_logo_max_width ); ?>px;">
												<span class="helper"></span><img src="<?php echo esc_url( $cg_logo ); ?>" alt="<?php bloginfo( 'name' ); ?>"/></a>
										</div>
<?php } else { ?>
										<div class="logo text-logo">
											<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
										</div>
									<?php } ?>
									<p class="sticky_cod">Cash on delivery</p>

								</div>	
								<div class="cg-wp-menu-wrapper">
									<?php if ( $logo_position == 'beside' ) { ?>
										<div class="cg-beside-search">
											<?php echo cg_product_search(); ?>
										</div>
									<?php } else { ?>
										<?php if ( $cg_display_cart !== 'no' ) { ?>
											<?php if ( $cg_catalog == 'disabled' ) { ?>
												<div class="cart-wrap">
													<?php if ( is_wc_active() ) {
														?>
														<?php echo cg_woocommerce_cart_dropdown(); ?>
													<?php }
													?>
												</div>
											<?php } ?>
										<?php }
									}
									?>

									<?php
									if ( isset( $cg_options['site_logo']['url'] ) ) {
										$cg_options['site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['site_logo']['url'] );
										$cg_logo						 = $cg_options['site_logo']['url'];
									}

									if ( $cg_logo ) {
										$cg_logo_width		 = $cg_options['site_logo']['width'];
										$cg_logo_max_width	 = $cg_logo_width / 2;

										if ( isset( $_GET['logo2'] ) ) {
											if ( !empty( $cg_options['cg_alt_site_logo']['url'] ) ) {
												$cg_options['cg_alt_site_logo']['url']	 = $protocol . str_replace( array( 'http:', 'https:' ), '', $cg_options['cg_alt_site_logo']['url'] );
												$cg_logo						 = $cg_options['cg_alt_site_logo']['url'];
												}

											if ( $cg_logo ) {
												$cg_logo_width		 = $cg_options['cg_alt_site_logo']['width'];
												$cg_logo_max_width	 = $cg_logo_width / 2;
											}
										}
										?>
<!--
										<div class="logo image">
											<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" style="max-width: <?php echo esc_attr( $cg_logo_max_width ); ?>px;">
												<span class="helper"></span><img src="<?php echo esc_url( $cg_logo ); ?>" alt="<?php bloginfo( 'name' ); ?>"/></a>
										</div>
-->
<?php } else { ?>
										<div class="logo text-logo">
											<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
										</div>
									<?php } ?>

									<?php if ( has_nav_menu( 'primary' ) ) { ?>
										<?php
										wp_nav_menu( array(
											'theme_location' => 'primary',
											'before'		 => '',
											'after'			 => '',
											'link_before'	 => '',
											'link_after'	 => '',
											'depth'			 => 4,
											'fallback_cb'	 => false,
											'walker'		 => new primary_cg_menu() )
										);
										?>
									<?php } else { ?>
										<p class="setup-message">You can set your main menu in <strong>Appearance &gt; Menus</strong></p>
<?php } ?>
								</div><!--/cg-wp-menu-wrapper -->
							</div><!--/container -->
						</div><!--/row -->
					</div><!--/cg-primary-menu -->
				</div><!--/cg-wp-menu-wrapper -->
			</div><!--/container -->
		</div><!--/cg-header-fixed -->
		<!-- open top bar here -->
		<div class="cg-shopping-toolbar cg-toolbar-fixed">
		    <div class="container">
		        <div class="row">
		            <div class="top-bar-left col-sm-12 col-md-7 col-lg-9">
						<?php if ( is_active_sidebar( 'top-bar-left' ) ) : ?>
							<?php dynamic_sidebar( 'top-bar-left' ); ?>
						<?php endif; ?>
		            </div>
		            <div class="col-sm-12 col-md-3 col-lg-3 visible-md visible-sm visible-xs mobile-search">
						<?php if ( is_active_sidebar( 'mobile-search' ) ) : ?>
							<?php dynamic_sidebar( 'mobile-search' ); ?>
						<?php endif; ?>
		            </div>
		            <div class="col-sm-3 col-md-2 col-lg-3 top-bar-right wpml cg-top-menu-beside">
						<?php if ( is_active_sidebar( 'top-bar-right' ) ) : ?>
							<?php dynamic_sidebar( 'top-bar-right' ); ?>
						<?php endif; ?>

						<?php
						if ( $cg_display_cart !== 'no' ) {
							if ( $cg_catalog == 'disabled' ) {
								echo cg_header_cart();
							}
						}
						?>

		            </div>

		        </div>
		    </div>
		</div>
		<!-- close top bar here -->

	</div><!--/cg-header-fixed-wrapper. -->

<?php }
?>

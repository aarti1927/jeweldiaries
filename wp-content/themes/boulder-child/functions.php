<?php
/**
 * CommerceGurus Child theme functions
 */

require_once 'inc/mailchimp/Mailchimp.php';


// Dequeue parent css
function cg_dequeue_parent_css() {
	wp_dequeue_style( 'cg-commercegurus' ); 
	wp_dequeue_style( 'cg-responsive' ); 
}

add_action( 'wp_enqueue_scripts', 'cg_dequeue_parent_css', 100 );

// Reorder parent css
function cg_reorder_child_css() {
	wp_dequeue_style( 'cg-style' );
	//wp_register_style( 'cg-child-styles', get_stylesheet_uri() );
	wp_enqueue_style( 'cg-commercegurus' );
	wp_enqueue_style( 'cg-responsive' );
	wp_enqueue_style( 'cg-child-styles',get_stylesheet_uri(),null,filemtime(get_stylesheet_directory().'/style.css') ,true);
}

add_action( 'wp_enqueue_scripts', 'cg_reorder_child_css', 101 );



add_action( 'wp_enqueue_scripts', 'register_my_scripts_and_styles' );
function register_my_scripts_and_styles(){
	wp_enqueue_script('parsley', get_stylesheet_directory_uri() . '/parsley.min.js',array('jquery'),'',true);
	wp_enqueue_script('jquery_sticky', get_stylesheet_directory_uri() . '/js/src/jquery.sticky-kit.min.js',true);
	wp_enqueue_script('common-js', get_stylesheet_directory_uri() . '/js/common.js',true);
      //wp_enqueue_script('stickymoto', get_stylesheet_directory_uri() . '/js/src/stickyMojo.js',true);
	
}



/**
 * Add checkbox field to the checkout1
 **/
add_action('woocommerce_after_order_notes', 'jd_checkout_fields');
 
function jd_checkout_fields( $checkout ) {
 
    echo '<div id="jd-checkoutfields">';
 
    woocommerce_form_field( 'terms_conditions', array(
        'type'          => 'checkbox',
        'class'         => array('input-checkbox'),
        'label'         => __('By proceeding to Checkout and placing the order, you acknowledge that your have read and agree to Jewel Diaries Terms of Use and Privacy Policy'),
        'required'  => true,
        ), $checkout->get_value( 'terms_conditions' ));
        
        
/*
            woocommerce_form_field( 'newsletter', array(
        'type'          => 'checkbox',
        'class'         => array('input-checkbox'),
        'label'         => __('By proceeding to checkout and placing the order, you agree to subscribe to the Jewel Diaries Newsletter.'),
        'required'  => false,
        ), $checkout->get_value( 'newsletter' ));
*/
 
    echo '</div>';
    
    
  
}
 
/**
 * Process the checkout
 **/
add_action('woocommerce_checkout_process', 'jd_checkout_fields_validation');
 
function jd_checkout_fields_validation() {
    global $woocommerce;
    // Check if set, if its not set add an error.
    if (!$_POST['terms_conditions'])
            wc_add_notice( '<strong>Please agree to our terms conditions in order to place your order.</strong>', 'error' );

}
 
/**
 * Update the order meta with field value
 **/
add_action('woocommerce_checkout_update_order_meta', 'jd_checkout_fields_process');
 
function jd_checkout_fields_process( $order_id ) {
    if ($_POST['terms_conditions']) update_post_meta( $order_id, 'terms_conditions', esc_attr($_POST['terms_conditions']));
/*
    if ($_POST['newsletter']) 
        {
            //Todo: subscribe user to Mailchimp list
            update_post_meta( $order_id, 'newsletter', esc_attr($_POST['newsletter']));
        }
*/
}

function custom_load_template($attrs)
{
	$default_attrs = array(
		'name' => '',
		'extension' => '.php'
	);
	
	$attrs = array_merge($default_attrs, (array)$attrs);
	
	if(trim($attrs['name']) != '')
	{
		locate_template($attrs['name'].$attrs['extension'], true, true);
	}
}
add_shortcode('custom-load-template', 'custom_load_template');

add_action('woocommerce_after_shop_loop_item_title','add_shop_now_button');
function add_shop_now_button()
{?>
  <div class="shopy">
	  <p>Shop Now</p>
	 </div>	
<?php
}

add_action('gform_validation_2','subsrcibe_mailchimp',10,2);
function subsrcibe_mailchimp($validation_result){
    $form = $validation_result['form'];
    $mc_api = new Mailchimp('e3c87de5c01b0a5ebce54eee9ac87eca-us11');
    $email = rgpost('input_1');
    try{
        $response = $mc_api->lists->subscribe('4750b41330',array('email'=>$email),null,'html',false,true,true,false);   
    }
    catch (Exception $e){
         foreach( $form['fields'] as &$field ) {
         $validation_result['is_valid'] = false;
            //NOTE: replace 1 with the field you would like to validate
            if ( $field->id == '1' ) {
                $field->failed_validation = true;
                $field->validation_message = $e->getMessage();;
                break;
            }
        }
    }
    $validation_result['form'] = $form;
    return $validation_result;
}



function payment_gateway_disable_country( $available_gateways ) {
	
	global $woocommerce;
	global $WOOCS;
	
   if($WOOCS->current_currency=='USD' && $woocommerce->customer->get_country() != 'IN' ) 
   {  
		unset( $available_gateways['ccavenue'] );
		unset( $available_gateways['cod'] );
	} else 
	{
		unset( $available_gateways['paypal_express'] );
	}
	
	return $available_gateways;
}
add_filter( 'woocommerce_available_payment_gateways', 'payment_gateway_disable_country' );

function checkout_country_fields($var, $billing) {
     
     global $WOOCS;
     global $woocommerce;
     if($WOOCS->current_currency=='INR')
     {   
	     	return 'IN'; 
     }
     else
	 {
		 	return (WC()->customer->get_country() && WC()->customer->get_country()!='IN') ? WC()->customer->get_country() : 'US';;
 	}  
 	
 	  
     
}

add_filter( 'default_checkout_country' , 'checkout_country_fields' );

add_filter('woocommerce_checkout_fields','change_currency_country');

function cg_widgets_init2() {

	register_sidebar( array(
		'name'			 => __( 'Sidebar', 'commercegurus' ),
		'id'			 => 'sidebar-1',
		'before_widget'	 => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</aside>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Top Toolbar - Left', 'commercegurus' ),
		'id'			 => 'top-bar-left',
		'before_widget'	 => '<div id="%1$s" class="%2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Top Toolbar - Right', 'commercegurus' ),
		'id'			 => 'top-bar-right',
		'before_widget'	 => '<div id="%1$s" class="%2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Mobile Search', 'commercegurus' ),
		'id'			 => 'mobile-search',
		'before_widget'	 => '<div id="%1$s" class="%2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Header Search', 'commercegurus' ),
		'id'			 => 'header-search',
		'before_widget'	 => '<div id="%1$s" class="%2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Shop Sidebar', 'commercegurus' ),
		'id'			 => 'shop-sidebar',
		'before_widget'	 => '<div id="%1$s" class="widget %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title"><span>',
		'after_title'	 => '</span></h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Below main body', 'commercegurus' ),
		'id'			 => 'below-body',
		'before_widget'	 => '<div id="%1$s" class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-sm-height col-full-height %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title">',
		'after_title'	 => '</h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'First Footer', 'commercegurus' ),
		'id'			 => 'first-footer',
		'before_widget'	 => '<div id="%1$s" class=" col-lg-4 col-md-4 col-sm-6 col-xs-12 col-nr-4 "><div class="inner-widget-wrap">',
		'after_widget'	 => '</div></div>',
		'before_title'	 => '<h4 class="widget-title">',
		'after_title'	 => '</h4>',
	) );

	register_sidebar( array(
		'name'			 => __( 'Second Footer', 'commercegurus' ),
		'id'			 => 'second-footer',
		'before_widget'	 => '<div id="%1$s" class="col-lg-4 col-md-4 col-sm-12 col-xs-12 col-nr-3 %2$s">',
		'after_widget'	 => '</div>',
		'before_title'	 => '<h4 class="widget-title">',
		'after_title'	 => '</h4>',
	) );
}

add_action( 'after_setup_theme', 'cg_widgets_init2' );
function change_currency_country($field){
   global $woocommerce;
   global $WOOCS;
                      
   $country = $woocommerce->customer->get_country();
 
   if($country == 'IN' && $WOOCS->current_currency=='USD') { //        
       
        WC()->customer->set_country( 'US' );
      
   } else if($country != 'IN' && $WOOCS->current_currency=='INR') { 
     // wc_print_notice( __( 'You can\'t Change your country as your currency is INR.', 'woocommerce' ), 'success' );
       WC()->customer->set_country('IN' );
   }
  
    
   return $field;

}


?>
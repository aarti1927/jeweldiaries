<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package commercegurus
 */
global $cg_options;
$cg_below_body_widget	 = '';
$cg_below_body_widget	 = $cg_options['cg_below_body_widget'];
$cg_footer_message		 = '';
$cg_footer_message		 = $cg_options['cg_footer_message'];
$cg_footer_top_active	 = '';
$cg_footer_top_active	 = $cg_options['cg_footer_top_active'];
$cg_footer_bottom_active = '';
$cg_footer_bottom_active = $cg_options['cg_footer_bottom_active'];
$cg_footer_cards_display = '';
$cg_footer_cards_display = $cg_options['cg_show_credit_cards'];
$cg_back_to_top			 = '';
$cg_back_to_top			 = $cg_options['cg_back_to_top'];

function display_card( $card, $status ) {
	if ( $card == '1' and $status == '1' ) {
		echo do_shortcode( '[cg_card type="visa"]' );
	}
	if ( $card == '2' and $status == '1' ) {
		echo do_shortcode( '[cg_card type="mastercard"]' );
	}
	if ( $card == '3' and $status == '1' ) {
		echo do_shortcode( '[cg_card type="paypal"]' );
	}
	if ( $card == '4' and $status == '1' ) {
		echo do_shortcode( '[cg_card type="amex"]' );
	}
}

if ( $cg_below_body_widget == 'yes' ) {
	?>
	<section class="below-body-widget-area">
		<div class="container">
			<div class="row">
				<div class="row-same-height row-full-height">
					<?php if ( is_active_sidebar( 'below-body' ) ) { ?>
						<?php dynamic_sidebar( 'below-body' ); ?>  
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
<?php } ?>

<footer class="footercontainer" id="footer" role="contentinfo"> 
	<div class="footer-background"></div>
	<?php if ( $cg_footer_top_active == 'yes' ) { ?>
		<?php if ( is_active_sidebar( 'first-footer' ) ) : ?>
			<div class="lightwrapper">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'first-footer' ); ?>   

					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.lightwrapper -->
		<?php endif; ?>
	<?php } ?>

	<?php if ( $cg_footer_bottom_active == 'yes' ) { ?>
		<?php if ( is_active_sidebar( 'second-footer' ) ) : ?>
			<div class="subfooter">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'second-footer' ); ?>            
					</div><!-- /.row -->
				</div><!-- /.container -->
			</div><!-- /.subfooter -->
		<?php endif; ?>
	<?php } ?>

</footer>
</div><!--/wrapper-->
</div><!-- close #cg-page-wrap -->

<div class="footer">
	<div class="container">
		<div class="row">
			<div class="bottom-footer-left col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php
				if ( class_exists( 'CGToolKit' ) ) {
					if ( $cg_footer_cards_display == 'show' ) {
						echo '<div class="footer-credit-cards">';
						$cg_card_array = ( $cg_options['cg_show_credit_card_values'] );
						foreach ( $cg_card_array as $card => $status ) {
							display_card( $card, $status );
						}
						echo '</div>';
					}
				}
				?>
				<?php
				if ( $cg_footer_message ) {
					$args = array(
					    //formatting
					    'strong' => array(),
					    'em'     => array(),
					    'b'      => array(),
					    'i'      => array(
					    	'class' => array()
					    	),
					    'p'		 => array(),

					    //links
					    'a'     => array(
					        'href' => array()
					    )
					);
					echo '<div class="footer-copyright">';
					echo wp_kses( $cg_footer_message, $args );
					echo '</div>';
				}
				?>
				<ul class="endbar_links">
					<li><a href="http://jeweldiaries.com/privacy-policy/">Privacy Policy</a></li>
					<li><a href="http://jeweldiaries.com/term-conditions/">Terms & Conditions</a></li>
<!--
					<li><a href="#">Payment Partners</a></li>
					<li><a href="#">Logistics Partners</a></li>
-->
				</ul>
			</div>

		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.footer -->

<?php if ( $cg_back_to_top == 'yes' ) { ?>
	<a href="#0" class="cd-top">Top</a>
<?php } ?>
<?php
global $cg_live_preview;
if ( isset( $cg_live_preview ) )
	include("live-preview.php")
	?>
	
	
<!-- Facebook Pixel Code -->

<script>
	jQuery(document).ready(function(){
	console.log(window.location.href);	
	if(window.location.href=='http://jeweldiaries.com/checkout/order-received/'){
	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
	document,'script','https://connect.facebook.net/en_US/fbevents.js');
	 
	fbq('init', '732524993544617');
	fbq('track', 'Purchase', {value: '0.00', currency:'INR'});
	}
	});
</script>

 <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=732524993544617&ev=PageView&noscript=1"/></noscript> 
 
<!-- End Facebook Pixel Code -->
			
			
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?3QP4ES6BPZ8Kb0PBfidN60G7k1YLMqTi";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<?php 
	if(is_page('checkout'))
	{ ?>
	<script type="text/javascript">
				jQuery(document).ready(function(){
					
					jQuery('#billing_country').change(function(){
						if(jQuery('#billing_country').val()=='IN' && current_currency=='USD')
						{
							jQuery('#billing_country').val('US');
							jQuery('#s2id_billing_country #select2-chosen-1').text('United States (US)');
							jQuery('#order_review').prepend('<div class="woocommerce-message custom-woocommerce-message">You can\'t change your country as India as your currency is USD.</div>');
						}
						else if(jQuery('#billing_country').val()!='IN' && current_currency=='INR'){
							jQuery('#billing_country').val('IN');
							jQuery('#s2id_billing_country #select2-chosen-1').text('India');
							//console.log(jQuery('#billing_country').val());
							jQuery('#order_review').prepend('<div class="woocommerce-message custom-woocommerce-message">You can\'t change your country as your currency is INR.</div>');
							
						}
						setTimeout(function(){jQuery('#order_review .custom-woocommerce-message').remove();}, 10000);
					});
					
				});
			</script>
			
		
			<?php } ?>

<?php wp_footer(); ?>
</body>
</html>